# README #

* This is a demo to add/update/delete user accounts and perform transactions between user accounts

### What is this repository for? ###
* The repo has been created for a demo. Rest endpoints are available to perform various operarions


### Tech stack ###
* java8,maven and springboot
* Please refer the pom.xml for dependencies

### Running the application ###
* http://localhost:9090 is the server port
* To view  all accounts GET: /accounts
* To add an account POST: /accounts/{name}/{amount}
* To update account balance PUT: /accounts/{id}/{amount}
* To delete an account DELTE : /accounts/{id}
* To make atransfer PUT : /transaction/{srcAcct}/{destAcct}/{amount}
* Test cases have been written to cover all uses cases.
* Run the SpringBootDemoApplication, followed by SpringBootDemoApplicationTests to execute the test cases
* 3 User accounts would be added to the in memory hashmap, as soon as the application is started
