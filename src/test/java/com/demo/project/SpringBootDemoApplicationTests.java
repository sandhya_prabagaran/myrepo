package com.demo.project;

import com.demo.project.controller.AccountController;
import com.demo.project.controller.TransactionController;
import com.demo.project.service.TransferService;
import com.demo.project.model.Account;
import com.demo.project.util.MessageConstants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@WebMvcTest({AccountController.class, TransactionController.class})
public class SpringBootDemoApplicationTests {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TransferService transferService;

	@Test
	public void testGetAccounts() throws Exception
	{
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get("/accounts");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		assertNotNull(response.getContentAsString());
	}


	@Test
	public void testCreateAccounts() throws Exception
	{
		Mockito.when(transferService.addAccount(Mockito.any(Account.class))).thenReturn(1);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/accounts/create/test/100.23");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
		assertEquals(MessageConstants.ACCOUNT_CREATED, response.getContentAsString());
	}

	@Test
	public void testCreateAccountInvalidName() throws Exception
	{
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/accounts/create/test12/100");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals(MessageConstants.ERROR_NAME_SPL_CHARS,response.getContentAsString());
	}

	@Test
	public void testCreateAccountInvalidAmount() throws Exception
	{
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/accounts/create/testing/100abc");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals(MessageConstants.BALANCE_INVALID_FORMAT, response.getContentAsString());
	}

	@Test
	public void testUpdateAccounts() throws Exception
	{
		Mockito.when(transferService.updateAccount(Mockito.any(),Mockito.any())).thenReturn("200.0");
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/accounts/update/1/100");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.ACCEPTED.value(), response.getStatus());
		assertEquals("200.0",response.getContentAsString());
	}

	@Test
	public void testUpdateAccountInvalidId() throws Exception
	{
		Mockito.when(transferService.updateAccount(Mockito.any(),Mockito.any())).thenReturn(null);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/accounts/update/000/100");
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals(MessageConstants.ACCOUNT_UPDATE_ERROR, response.getContentAsString());
	}

	@Test
	public void testUpdateAccountInvalidAmount() throws Exception
	{
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/accounts/update/1/100abc");
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals(MessageConstants.BALANCE_INVALID_FORMAT, response.getContentAsString());
	}

	@Test
	public void testDeleteAccount() throws Exception
	{
		Mockito.when(transferService.deleteAccount(Mockito.anyInt())).thenReturn(1);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.delete("/accounts/delete/1");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.ACCEPTED.value(), response.getStatus());
		assertEquals(MessageConstants.ACCOUNT_DELETED, response.getContentAsString());
	}

	@Test
	public void testDeleteAccountInvalidId() throws Exception
	{
		Mockito.when(transferService.deleteAccount(Mockito.anyInt())).thenReturn(0);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.delete("/accounts/delete/000");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
	}

	@Test
	public void testUpdateTransaction() throws Exception
	{
		Mockito.when(transferService.transferAccounts(Mockito.anyInt(),Mockito.anyInt(),Mockito.any())).thenReturn(1);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/transaction/1/2/100.0");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.ACCEPTED.value(), response.getStatus());
		assertEquals(MessageConstants.TRANSACTION_COMPLETED, response.getContentAsString());
	}

	@Test
	public void testTransactionInvalidAcct() throws Exception
	{
		Mockito.when(transferService.transferAccounts(Mockito.anyInt(),Mockito.anyInt(),Mockito.any())).thenReturn(0);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/transaction/0/2/100.0");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals(MessageConstants.INVALID_TRANSACTION, response.getContentAsString());
	}

	@Test
	public void testTransactionInsufficientBalance() throws Exception
	{
		Mockito.when(transferService.transferAccounts(Mockito.anyInt(),Mockito.anyInt(),Mockito.any())).thenReturn(-1);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/transaction/1/2/1000");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals(MessageConstants.INSUFFICIENT_BALANCE, response.getContentAsString());
	}

	@Test
	public void testTransactionInvalidAmount() throws Exception
	{
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/transaction/1/2/abc");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals(MessageConstants.BALANCE_INVALID_FORMAT, response.getContentAsString());
	}

}
