package com.demo.project.controller;

import com.demo.project.model.Account;
import com.demo.project.service.TransferService;
import com.demo.project.util.MessageConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;

@RestController
@RequestMapping(path = "/accounts")
public class AccountController
{
    @Autowired
    private TransferService transferService;

    /**
     *View all available accounts
     * @return
     */
    @GetMapping(produces = "application/json")
    public ResponseEntity<?> getAccounts()
    {
        return ResponseEntity.ok().body(transferService.getAllAccounts());
    }

    /**
     * add a new user account
     * @param name
     * @param amount
     * @return
     */
    @PostMapping(path= "/create/{name}/{amount}", produces = "application/json")
    public ResponseEntity<Object> addAccount(@PathVariable(required = true) String name,
                                             @PathVariable(required = true) String amount) {

        try {
            int id = 0;

            if ((!name.equals(null)) && name.matches("^[a-zA-Z]*$")) {
                //add resource
                id = transferService.addAccount(new Account(name, new BigDecimal(amount)));
                //Create resource location
                URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(id)
                        .toUri();

                //Send location in response
                return ResponseEntity.created(location).body(MessageConstants.ACCOUNT_CREATED);
            }
            return ResponseEntity.badRequest().body(MessageConstants.ERROR_NAME_SPL_CHARS);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(MessageConstants.BALANCE_INVALID_FORMAT);
        }
    }

    /**
     * Update balance for an existing user account
     * @param id
     * @param amount
     * @return
     */
    @PutMapping(path= "/update/{id}/{amount}", produces = "application/json")
    public ResponseEntity<Object> updateAccount(@PathVariable(required = true) Integer id,
                                                @PathVariable(required = true) String amount) {

        try {
            String balance = transferService.updateAccount(id, new BigDecimal(amount));
            return balance!=null? ResponseEntity.accepted().body(balance): ResponseEntity.badRequest().body(MessageConstants.ACCOUNT_UPDATE_ERROR);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(MessageConstants.BALANCE_INVALID_FORMAT);
        }
    }

    /**
     * Delete an existing user account
     * @param id
     * @return
     */
    @DeleteMapping(path= "/delete/{id}", produces = "application/json")
    public ResponseEntity<Object> deleteAccount(@PathVariable(required = true) Integer id) {
        int responseCode = transferService.deleteAccount(id);
        return responseCode!=0? ResponseEntity.accepted().body(MessageConstants.ACCOUNT_DELETED): ResponseEntity.notFound().build();
    }
}
