package com.demo.project.controller;

import com.demo.project.service.TransferService;
import com.demo.project.util.MessageConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping(path = "/transaction")
public class TransactionController
{
    @Autowired
    private TransferService transferService;

    /**
     * Make a transaction between 2 existing user accounts
     * @param srcAcct
     * @param destAcct
     * @param amount
     * @return
     */
    @PutMapping(path= "/{srcAcct}/{destAcct}/{amount}")
    public ResponseEntity<Object> updateTransaction(@PathVariable(required = true) Integer srcAcct,
                                                    @PathVariable(required = true) Integer destAcct,
                                                    @PathVariable(required = true) String amount) {

        try {
            int responseCode = transferService.transferAccounts(srcAcct,destAcct, new BigDecimal(amount));
            if(1==responseCode){
                return ResponseEntity.accepted().body(MessageConstants.TRANSACTION_COMPLETED);
            }else if(-1 == responseCode) {
                return ResponseEntity.badRequest().body(MessageConstants.INSUFFICIENT_BALANCE);
            }
            return ResponseEntity.badRequest().body(MessageConstants.INVALID_TRANSACTION);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(MessageConstants.BALANCE_INVALID_FORMAT);
        }
    }

}
