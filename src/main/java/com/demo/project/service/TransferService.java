package com.demo.project.service;

import com.demo.project.model.Account;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class TransferService
{
    public static Map<Integer,Account> accountsMap = new HashMap<>();

    static{
        accountsMap.put(1,new Account(1,"acct1",new BigDecimal(100.0)));
        accountsMap.put(2,new Account(2,"acct2",new BigDecimal(500.50)));
        accountsMap.put(3,new Account(3,"acct3",new BigDecimal(56780.05)));
    }


    /**
     *
     * @return
     */
    public Collection<Account> getAllAccounts()
    {
        return accountsMap.values();
    }

    /**
     * @param account
     * @return
     */
    public int addAccount(Account account) {
        //Generate resource id
        int id = accountsMap.size() != 0 ? accountsMap.size() + 1 : 1;
        account.setId(id);
        accountsMap.put(id,account);
        return id;
    }

    /**
     *
     * @param accountId
     * @return
     */
    public int deleteAccount(int accountId) {
        Account delAcct = validateAccount(accountId);
        if(null != delAcct){
            accountsMap.remove(delAcct);
            return 1;
        }else{return 0;}
    }

    /**
     *
     * @param id
     * @param amount
     * @return
     */
    public String updateAccount(Integer id,BigDecimal amount) {
        Account currentAcct = validateAccount(id);
        if(null != currentAcct){
            BigDecimal newBalance = currentAcct.getBalance().add(amount);
            currentAcct.setBalance(newBalance);
            accountsMap.put(currentAcct.getId(),currentAcct);
            return currentAcct.getBalance().toString();
        }else{return null;}
    }

    /**
     *
     * @param srcAcctId
     * @param destAcctId
     * @param amount
     * @return
     */
    public int transferAccounts(int srcAcctId,int destAcctId,BigDecimal amount) {
        int validTxn=0;
        Account srcAccount = validateAccount(srcAcctId);
        Account destAccount = validateAccount(destAcctId);
        if(null != srcAccount && null != destAccount){
            validTxn = validBalance(srcAccount,amount);
            if(validTxn>0){
                srcAccount.setBalance(srcAccount.getBalance().subtract(amount));
                destAccount.setBalance(destAccount.getBalance().add(amount));
                accountsMap.put(srcAcctId,srcAccount);
                accountsMap.put(destAcctId,destAccount);
            }
        }
        return  validTxn;
    }

    /**
     *
     * @param acctId
     * @return
     */
    private Account validateAccount(int acctId){
        return !accountsMap.isEmpty() && accountsMap.containsKey(acctId) ?accountsMap.get(acctId):null;
    }

    private int validBalance(Account srcAccount,BigDecimal amount){
        return srcAccount.getBalance().compareTo(amount);
    }
}
