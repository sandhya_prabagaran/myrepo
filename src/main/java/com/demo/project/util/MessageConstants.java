package com.demo.project.util;

public class MessageConstants {

    public static final String ACCOUNT_CREATED = "Account created";

    public static final String ERROR_NAME_SPL_CHARS = "Please provide name without special characters";

    public static final String BALANCE_INVALID_FORMAT = "Please provide a valid balance";

    public static final String ACCOUNT_UPDATE_ERROR = "Error in account update";

    public static final String ACCOUNT_DELETED = "Account deleted";

    public static final String TRANSACTION_COMPLETED = "Transaction completed";

    public static final String INSUFFICIENT_BALANCE = "Insufficient balance";

    public static final String INVALID_TRANSACTION = "Invalid transaction";

}
