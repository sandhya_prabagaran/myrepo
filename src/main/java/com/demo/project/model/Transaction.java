package com.demo.project.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Transaction {


    public Transaction(Integer srcAcct, int destAcct,BigDecimal transferAmount) {
        this.srcAcct = srcAcct;
        this.destAcct = destAcct;
        this.transferAmount=transferAmount;
    }

    private Integer srcAcct;
    private Integer destAcct;
    private BigDecimal transferAmount;

    public Integer getSrcAcct() {
        return srcAcct;
    }

    public Integer getDestAcct() {
        return destAcct;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return srcAcct.equals(that.srcAcct) && destAcct.equals(that.destAcct) && transferAmount.equals(that.transferAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(srcAcct, destAcct, transferAmount);
    }

    @Override
    public String toString() {
        return "Transfer [srcAcct=" + srcAcct + ",destAcct=" + destAcct +",transferAmount=" + transferAmount + "]";
    }
}
